#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/wait.h>
#include <limits.h>
#include <fcntl.h>
#include <stdlib.h>

static bool d1 = false;
static bool d2 = false;
static bool d3 = false;
pid_t coproc(char *cmd, int fd[2]);

int main(int argc, char **argv)
{
    int fd[2];
    pid_t pid;
    int fdr = STDIN_FILENO;
    if (argc == 2)
    {
        pid = coproc(argv[1], fd);
    }
    else if (argc == 3)
    {
        if ((fdr = open(argv[2], O_RDONLY)) < 0)
        {
            fprintf(stderr, "ERROR opening file '%s'!", argv[2]);
            return -1;
        }
        pid = coproc(argv[1], fd);
    }
    else
    {
        pid = coproc("ls -al", fd);
    }

    //read from file or stdin
    if (d1) printf("Starting read/write to child shell!!\n");
    char buf[PIPE_BUF];
    if (d1) printf("Reading from fd '%d' now!\n", fdr);
    ssize_t n;
    while ((n = read(fdr, buf, PIPE_BUF)) > 0)
    {
        //write to child process
        if (d1) printf("Printing '%s' to pipe!\n", buf);
        write(fd[0], buf, n);
    }
    if (d1) printf("Closing fd[0]!\n");
    close(fd[0]);

    while ((n = read(fd[1], buf, PIPE_BUF)) > 0)
    {
        //write to stdout
        if (d3) printf("writing '%s'! to stdout!\n", buf);

        //The problem was that I always wrote PIPE_BUF bytes & not n bytes
        //So on the last write it would still write the whole buffer again
        //which understandably writes a whole lot of random garbage.
        write(STDOUT_FILENO, buf, n);
    }
    if (d1) printf("Closing fd[1]!\n");
    close(fd[1]);

    //for some reason closing the pipe doesn't send EOF...

    if (d2) printf("waiting for child...\n");
    //wait until child wraps-up
    if ((waitpid (pid, NULL, 0)) < 0)
    {
        fprintf(stderr, "ERROR waiting for child!");
        return -2;
    }

    if (d2) printf("coproc returned %d!\n", pid);
    return 0;
}

int initPipes(int parent2child[2], int child2parent[2], int fd[2])
{
    if (pipe(child2parent) < 0)
    {
        fprintf(stderr, "ERROR creating child2parent pipe!\n");
        return -1;
    }
    if (d2) printf("child2parent pipe creation successful!\n");
    fd[1] = child2parent[0];

    if (pipe(parent2child) < 0)
    {
        fprintf(stderr, "ERROR creating parent2child pipe!\n");
        return -2;
    }
    if (d2) printf("parent2child pipe creation successful!\n");
    fd[0] = parent2child[1];
    return 0;
}

pid_t coproc(char *cmd, int fd[2])
{
    int parent2child[2];
    int child2parent[2];
    signal(SIGPIPE, SIG_IGN);

    int rv;
    if ((rv = initPipes(parent2child, child2parent, fd)) < 0)
    {
        return rv;
    }

    if (d2) printf("Forking!\n");
    pid_t pid = fork();
    if (pid < 0)
    {
        fprintf(stderr, "ERROR forking!");
    }
    else if (pid == 0)
    {
        if (d1) printf("Now in child process!\n");
        //child process execution
        //(open file descriptors are always inherited from parent process unless otherwise specified)
        close(parent2child[1]);
        close(child2parent[0]); //close child->parent read pipe (cannot write while reading is still possible & doesn't make sense)
        dup2(parent2child[0], STDIN_FILENO); //set input of child process to read pipe of parent->child
         //close parent->child write pipe (cannot read while writing is still possible & doesn't make sense)
        dup2(child2parent[1], STDOUT_FILENO);
        close(parent2child[0]);
        close(parent2child[1]);

        if (d1) printf("Executing command '%s'!\n", cmd);
        if (!execl("/bin/sh", "sh" , "-c", cmd, NULL))
        {
            fprintf(stderr, "ERROR executing command %s\n", cmd);
            return -1;
        }
        return 0;
    }
    else
    {
        if (d1) printf("Now in parent process!\n");
        //parent process execution
        close(parent2child[0]);
        close(child2parent[1]);
        if (d1) printf("Exiting parent process!\n");
        return pid;
    }
    return 0;
}
